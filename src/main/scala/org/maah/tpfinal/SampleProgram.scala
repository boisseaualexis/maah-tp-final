package org.maah.tpfinal

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.maah.tpfinal.config.ConfigParser
import org.maah.tpfinal.reader.SparkReaderWriter
import org.maah.tpfinal.services.Service1


object SampleProgram {


  def elementLePlusProche(elem1: Int, elem2: Int, comparedNumber: Int): Int = {
    if(Math.abs(elem1 - comparedNumber) > Math.abs(elem2 - comparedNumber))
      elem2
    else
      elem1
  }

  def majuscule(s: String): String = {
    if(s.contains("Barca"))
      s.toUpperCase
    else
      s
  }

  def keepWhenContains(element: String, stringToFind: String): Boolean = {
    if(element.contains(stringToFind))
      true
    else
      false
  }
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.OFF)
    val configCli: ConfigParser = ConfigParser.getConfigArgs(args)

    implicit val spark: SparkSession = SparkSession.builder().master("local").getOrCreate()

    val df = SparkReaderWriter.readData(configCli.inputPath, configCli.inputFormat)
    configCli.service match {
      case "service1" =>  val result = Service1.filterClientId(df, configCli.clientId)
        SparkReaderWriter.writeData(result, configCli.outputPath, configCli.outputFormat)
      //case "service2" => Service2.implemService2()
      //case "service3" => ???
      case _ => {
        println("Service non reconnu")
        sys.exit(1)
      }
    }




  }
}